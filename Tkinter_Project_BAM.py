import tkinter as tk 
import tkinter.messagebox
from tkinter import ttk 
from tkinter import *
from tkinter import scrolledtext
from tkinter.filedialog import askopenfilename, asksaveasfilename
from PIL import Image, ImageTk
from tkinter import messagebox
import csv

array=[]
arrayinput=[]

class tkinterApp(tk.Tk): 
      
    def __init__(self, *args, **kwargs):  
        
        tk.Tk.__init__(self, *args, **kwargs) 

        container = tk.Frame(self)   
        container.pack(side = "top", fill = "both", expand = True)  
         
        container.grid_rowconfigure(0, weight =1) 
        container.grid_columnconfigure(0, weight = 1) 
        self.frames = {}   
        for F in (StartPage,Menu,Add_client,Find_client,Delete_cLient,View_cLients,Controlo_txt):
            
            frame = F(container, self) 
            self.frames[F] = frame  
            frame.grid(row = 0, column = 0, sticky ="nsew") 
        self.show_frame(StartPage) 


        menubar = tk.Menu(self)        
        subMenu = tk.Menu(menubar)
        subMenu1 = tk.Menu(menubar)
        subMenu2 = tk.Menu(menubar)
        subMenu.add_command(label='Add CLient',command = lambda : self.show_frame(Add_client))
        subMenu.add_separator()
        subMenu.add_command(label='Find Client',command = lambda : self.show_frame(Find_client))
        subMenu.add_separator()
        subMenu.add_command(label='Delete CLient',command = lambda : self.show_frame(Delete_cLient))
        subMenu.add_separator()
        subMenu.add_command(label='View CLient')
        subMenu.add_separator()
        menubar.add_cascade(label='Menu',menu=subMenu)
  

        # def funcao_botao5(self):
        #     a=Controlo_txt()
        #     a.openfile()
            
        menubar.add_cascade(label='Files',menu=subMenu1)
        subMenu1.add_command(label='Open/Save csv',command = lambda : self.show_frame(Controlo_txt))
        subMenu1.add_separator()


            
            
        def infos():
            messagebox.showinfo( "Info", ' Version 1.0-2020' '\n' 'Developer: Duarte Valente')
        menubar.add_cascade(label='Info',command=infos)
        self.config(menu=menubar)
        
    def show_func_frame(self, cont): 
        frame1 = self.frames[cont]
        frame1.open_file()
        frame1.tkraise()
        
        
    def show_frame(self, cont): 
        frame = self.frames[cont]
        frame.tkraise() 

class StartPage(tk.Frame): 
    
    def __init__(self, parent, controller):  
        tk.Frame.__init__(self, parent) 
        #self.configure(bg='black')  

        
        IMAGE_PATH = 'bank2.jpg'
        WIDTH, HEIGTH = 500, 500       
        canvas = tk.Canvas(self, width=WIDTH, height=HEIGTH)
        canvas.pack()      
        img = ImageTk.PhotoImage(Image.open(IMAGE_PATH).resize((500, 500), Image.ANTIALIAS))
        canvas.background = img  # Keep a reference in case this code is put in a function.
        bg = canvas.create_image(0, 0, anchor=tk.NW, image=img)

        label = ttk.Label(self, text ="Bank Accont Management", font = ("Time", 22 ))
        label.place(x=100,y=200)
        
        
   
        button1 = ttk.Button(self, text ="Start Program",command = lambda : controller.show_frame(Menu)) 
        button1.place(x=210,y=300)
           
class Menu(tk.Frame): 
      
    def __init__(self, parent, controller): 
        

        tk.Frame.__init__(self, parent)
        
        IMAGE_PATH = 'bank.jpg'
        WIDTH, HEIGTH = 500, 500       
        canvas = tk.Canvas(self, width=WIDTH, height=HEIGTH)
        canvas.pack()      
        img = ImageTk.PhotoImage(Image.open(IMAGE_PATH).resize((500, 500), Image.ANTIALIAS))
        canvas.background = img  # Keep a reference in case this code is put in a function.
        bg = canvas.create_image(0, 0, anchor=tk.NW, image=img)
        
        
        label = ttk.Label(self, text ="MENU", font = ("Arial", 35 )) 
        label.place(x=170, y=50)  
        
        self.configure(bg='Black')
 
        button1 = ttk.Button(self, text ="Add Client", command = lambda : controller.show_frame(Add_client)) 
        button1.place(x=200, y=200)

   
        button2 = ttk.Button(self, text ="Find Client", command = lambda : controller.show_frame(Find_client))  
        button2.place(x=200, y=250)

   
        button3 = ttk.Button(self, text ="Delete CLient", command = lambda : controller.show_frame(Delete_cLient))  
        button3.place(x=200, y=300)

        button4 = ttk.Button(self, text ="View CLients", command = lambda : controller.show_frame(View_cLients)) 
        button4.place(x=200, y=350)

        
        button5 = ttk.Button(self, text ="Files Control", command = lambda : controller.show_frame(Controlo_txt))
        button5.place(x=200, y=400)
        

        
        button6 = ttk.Button(self, text ="Exit",command=qExit)
        button6.place(x=200, y=450)
        
class Add_client(tk.Frame):  
    def __init__(self, parent, controller): 
        global array
        tk.Frame.__init__(self, parent) 
        label = ttk.Label(self, text ="Add Client", font = ("Arial", 35 )) 
        label.place(x=150, y=50)
        name_var=tk.StringVar() 
        passw_var=tk.StringVar() 
        telef_var=tk.StringVar()
        saldo_var=tk.StringVar()
       
        def submit(): 
            name=name_entry.get() 
            password=passw_var.get() 
            telefone=telef_var.get()
            saldo=saldo_var.get()
                           
  
            cli=[name,password,telefone,saldo]
            array.append(cli)
            print(array)
            
            name_var.set("") 
            passw_var.set("") 
            telef_var.set("")
            saldo_var.set("")
               
        name_label = tk.Label(self, text = 'Client name: ', font=('calibre',10, 'bold'))
        name_label.place(x=100, y=150)
        name_entry = tk.Entry(self, textvariable = name_var,font=('calibre',10,'normal')) 
        name_entry.place(x=250, y=150)

        passw_label = tk.Label(self,text = 'ID number: ', font = ('calibre',10,'bold'))
        passw_label.place(x=100, y=180)
        passw_entry=tk.Entry(self, textvariable = passw_var,font = ('calibre',10,'normal')) 
        passw_entry.place(x=250, y=180)
        
        telef_label = tk.Label(self,text = 'Telephone: ', font = ('calibre',10,'bold')) 
        telef_label.place(x=100, y=210)
        telef_entry=tk.Entry(self, textvariable = telef_var,font = ('calibre',10,'normal')) 
        telef_entry.place(x=250, y=210)

        saldo_label = tk.Label(self,text = 'Bank account balance: ', font = ('calibre',10,'bold')) 
        saldo_label.place(x=100, y=240)
        saldo_entry=tk.Entry(self, textvariable = saldo_var,font = ('calibre',10,'normal')) 
        saldo_entry.place(x=250, y=240)
       
        sub_btn=tk.Button(self,text = 'Add',command = submit) 
        sub_btn.place(x=250, y=300)
           
       
        
        button1=ttk.Button(self, text ="Menu", command = lambda : controller.show_frame(Menu))
        button1.place(x=400, y=450)

class Find_client(tk.Frame):  
    def __init__(self, parent, controller): 
        tk.Frame.__init__(self, parent) 
        global array
        contproc_var=tk.StringVar() 
        
        def procurar():
            txtcont.delete('1.0',END)
            print(array)            
            cont=contproc_entry.get() 
            for imprimi_arrays_indiv in array:  
                if imprimi_arrays_indiv[1] == cont:
                    for lista_arrays_indiv in imprimi_arrays_indiv:   
                        txtcont.insert(END,lista_arrays_indiv)
                        txtcont.insert(END,'\n')        
            
 
        f0=Frame(self,width=500, height=100,bd=2,relief='raise')
        f0.pack(side=TOP)
        label = ttk.Label(f0, text ="Find Client", font = ("Arial", 35 )) 
        label.place(x=150, y=10)

    
        f1=Frame(self,width=234, height=500,bd=2,relief='raise')
        f1.pack(side=LEFT)
        name_label = ttk.Label(f1, text = 'ID to search: ', font=('calibre',10, 'bold'))
        name_label.place(x=30, y=120)
        contproc_entry=tk.Entry(f1, textvariable = contproc_var,font = ('calibre',13,'normal')) 
        contproc_entry.place(x=30, y=150)

        
        f2=Frame(self,width=100, height=500,bd=2,relief='raise')
        f2.pack(side=LEFT)
        button1=ttk.Button(f2, text ="Search", command = procurar)
        button1.place(x=0, y=150)
        
        f3=Frame(self,width=166, height=500,bd=2,relief='raise')
        f3.pack(side=LEFT)

        txtcont=Text(f3,width=19,height=20,bg='white',bd=1)
        txtcont.place(x=2, y=0)
        
        button1=ttk.Button(self, text ="Menu", command = lambda : controller.show_frame(Menu))
        button1.place(x=400, y=450)
              
class Delete_cLient(tk.Frame):  
    def __init__(self, parent, controller): 
        tk.Frame.__init__(self, parent) 
        
        putcont_var=tk.StringVar() 
        global array
        def delete():
            txtcont.delete('1.0',END)
            
            contri=putcontr_entry.get() 
            for imprimi_arrays_indiv in array:  
                if imprimi_arrays_indiv[1]==contri:
                    array.remove(imprimi_arrays_indiv)

        def visualizar():
            global array
            for numero_cliente in array:
                print(numero_cliente)
                txtcont.insert(END,numero_cliente)
                txtcont.insert(END,'\n')
        
        
        label = ttk.Label(self, text ="Delete client", font = ("Arial", 35 )) 
        label.place(x=150, y=50)
        
        contriapagar_label = tk.Label(self, text = 'Write the customer ID you want to delete: ', font=('calibre',10, 'bold'))
        contriapagar_label.place(x=100, y=150)
        putcontr_entry = tk.Entry(self, textvariable = putcont_var,font=('calibre',10,'normal')) 
        putcontr_entry.place(x=100, y=200)
        
        button1=ttk.Button(self, text ="Click to delete this ID",command = delete)
        button1.place(x=250,y=200)
        
        txtcont=Text(self,width=50,height=5,bg='white',bd=1)
        txtcont.place(x=50, y=250)
        
        button_update=ttk.Button(self, text ="Update/View DataBase",command=visualizar)
        button_update.place(x=210, y=350)
    
    
        button1=ttk.Button(self, text ="Menu", command = lambda : controller.show_frame(Menu))
        button1.place(x=400, y=450)

class Controlo_txt(tk.Frame):  
    def __init__(self, parent, controller): 
        global array
        global arrayinput
        tk.Frame.__init__(self, parent) 
        label = ttk.Label(self, text ="Files Control", font = ("Arial", 35 )) 
        label.place(x=130, y=50)
        
        
        
        def save_file():
            filepath = asksaveasfilename(
                defaultextension="csv",
                filetypes=[("Text Files", "*.csv")],
            )
            if not filepath:
                return              
            with open(filepath, 'w',newline='') as csvfile:          
                    x = csv.writer(csvfile, delimiter='\t') 
                    for escreve_linha_csv in array:
                        x.writerow(escreve_linha_csv)
                    tkinter.messagebox.showinfo(title='Info', message='You just EXPORT your information to a CSV file')
        
      
                
        def open_file():
            filepath = askopenfilename(
                filetypes=[("Text Files", "*.csv")]
            )
            if not filepath:
                return
            with open(filepath, "r") as csvfile:
                le_array_csv = csv.reader(csvfile, delimiter='\t')
                linhas_no_csv=len(list(csv.reader(open(filepath))))
                if linhas_no_csv>=1:
                    for linha_cliente in le_array_csv:
                        print(linha_cliente) 
                        array.append(linha_cliente)
                    tkinter.messagebox.showinfo(title='Info', message='Acabou de fazer IMPORT da sua informação de um ficheiro CSV')
    
        btn_open = ttk.Button(self, text="Open", command=open_file)
        btn_open.place(x=200, y=200)
        btn_save = ttk.Button(self, text="Save As", command=save_file)
        btn_save.place(x=200, y=300)
        
        button1=ttk.Button(self, text ="Menu", command = lambda : controller.show_frame(Menu))
        button1.place(x=400, y=450)
              
class View_cLients(tk.Frame):  
    def __init__(self, parent, controller): 
        global array
        global arrayinput
        print(array)
        tk.Frame.__init__(self, parent) 
        label = ttk.Label(self, text ="View Clients", font = ("Arial", 20 )) 
        label.place(x=180, y=5)
      
        txt_edit=Text(self,width=50,height=22,bg='white',bd=1)        
        txt_edit.place(x=50, y=50)
        
        def visualizar():
            txt_edit.delete('1.0',END)
            print(array)
            for numero_cliente in array:
                print(numero_cliente)
                txt_edit.insert(END,numero_cliente)
                txt_edit.insert(END,'\n')
                
               
                    
       
        button2=ttk.Button(self, text ="Update/View Database", command = visualizar)
        button2.place(x=50, y=450)        
        button1=ttk.Button(self, text ="Menu", command = lambda : controller.show_frame(Menu))
        button1.place(x=400, y=450)
        
def qExit():
    qExit=tkinter.messagebox.askyesno('Quit System','Do you want to Quit')
    
    if qExit> 0:
        app.destroy()
        return
        
if __name__ == "__main__":
    
    app = tkinterApp()
    app.title("Bank Accont Management")
    app.geometry('500x500')
    app.resizable(width=False, height=False) 
    
    app.mainloop()



